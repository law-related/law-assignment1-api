from django.shortcuts import render
import requests
from django.http import JsonResponse

# Create your views here.



def get_films(request,film_name):
    default_url = f"http://www.omdbapi.com/?apikey=7485328a&s={film_name}/"
    url = default_url
    datas = requests.get(url).json()
    return JsonResponse(data=datas)

