from django.apps import AppConfig


class LawA1ApiServiceConfig(AppConfig):
    name = 'LAW_A1_API_Service'
