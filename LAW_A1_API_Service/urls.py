from  django.urls import path
from  LAW_A1_API_Service.views import *

urlpatterns = [
    path('get-films/<str:film_name>/', get_films, name='get-films'),
]